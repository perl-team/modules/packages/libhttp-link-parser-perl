libhttp-link-parser-perl (0.200-2) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/*: update URLs from {search,www}.cpan.org to MetaCPAN.

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Set upstream metadata fields: Bug-Database, Repository, Repository-
    Browse.

  [ gregor herrmann ]
  * Remove Makefile.old via debian/clean. (Closes: #1047060)

 -- gregor herrmann <gregoa@debian.org>  Wed, 06 Mar 2024 15:50:54 +0100

libhttp-link-parser-perl (0.200-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Wed, 06 Jan 2021 17:28:30 +0100

libhttp-link-parser-perl (0.200-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ Florian Schlichting ]
  * Import Upstream version 0.200
  * Drop copyright paragraphs for inc/* modules no longer distributed by
    upstream
  * Bump copyright years
  * Demote RDF::Trine to Recommends
  * Bump debhelper dependency and compat level to 8
  * Add build-dependency on Test::More 0.96
  * Email change: Florian Schlichting -> fsfs@debian.org
  * Declare compliance with Debian Policy 3.9.5
  * Update Vcs-* fields to their canonical value

 -- Florian Schlichting <fsfs@debian.org>  Mon, 20 Jan 2014 22:17:57 +0100

libhttp-link-parser-perl (0.103-1) unstable; urgency=low

  [ Florian Schlichting ]
  * Imported Upstream version 0.103.
  * Bumped copyright years, adjusted DEP-5 headers and added/deleted stanzas
    for new and removed files under inc/.
  * Added myself to Uploaders and copyright.
  * Added dependency on libhttp-message-perl.

  [ Jonas Smedegaard ]
  * Update copyright file: Improve references for convenience copy of
    Module::Install.

  [ gregor herrmann ]
  * Remove debian/source/local-options; abort-on-upstream-changes and
    unapply-patches are default in dpkg-source since 1.16.1.

 -- Florian Schlichting <fschlich@zedat.fu-berlin.de>  Sat, 17 Dec 2011 21:15:22 +0100

libhttp-link-parser-perl (0.101-2) unstable; urgency=low

  * Improve package relations:
    + Stop declaring package part of core Perl even in oldstable:
      - libtest-simple-perl
      - libscalar-list-utils-perl
      - libencode-perl
  * Update copyright file:
    + Rewrite using draft 174 of DEP-5 format.
    + Rewrap license fields at 72 chars, and shorten comments.
  * Bump policy compliance to standards-version 3.9.2.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 08 May 2011 20:44:54 +0200

libhttp-link-parser-perl (0.101-1) unstable; urgency=low

  * Initial packaging release.
    Closes: bug#616514.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 05 Mar 2011 11:16:18 +0100
